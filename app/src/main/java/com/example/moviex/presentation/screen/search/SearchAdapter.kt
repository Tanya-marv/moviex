package com.example.moviex.presentation.screen.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.moviex.R

class SearchAdapter(
    private val clickListener: (SearchItem) -> Unit
) : RecyclerView.Adapter<SearchViewHolder>() {

    private val movies = mutableListOf<SearchItem>()

    override fun getItemCount(): Int = movies.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_search, parent, false)
        return SearchViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(movies[position], clickListener)
    }

    fun addMovies(newMovies: List<SearchItem>) {
        movies.addAll(newMovies)
        notifyDataSetChanged()
    }
}

class SearchViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(movies: SearchItem, clickListener: (SearchItem) -> Unit) {
        view.setOnClickListener { clickListener(movies) }
        view.findViewById<ImageView>(R.id.iv_poster)
            .load("https://image.tmdb.org/t/p/w342${movies.path}")
        view.findViewById<TextView>(R.id.tv_title).text = movies.title
        view.findViewById<TextView>(R.id.tv_data).text = movies.date
        view.findViewById<RatingBar>(R.id.rb_movies).rating = movies.rate / 2
    }
}

data class SearchItem(
    val id:Long,
    val path: String,
    val rate: Float,
    val title: String,
    val date: String
)
