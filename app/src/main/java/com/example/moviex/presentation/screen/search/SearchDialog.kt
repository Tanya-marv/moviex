package com.example.moviex.presentation.screen.search

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.moviex.presentation.routing.Router
import com.example.moviex.util.delegate.RouterDelegate

class SearchDialog : DialogFragment() {

    private val router by RouterDelegate()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val input = EditText(context)

            input.apply {
                hint = "Movie name"
                inputType = InputType.TYPE_CLASS_TEXT
            }

            AlertDialog.Builder(it).apply {
                setMessage("Start game?")
                setView(input)
                setPositiveButton("Search",
                    DialogInterface.OnClickListener { dialog, id ->
                        val input = input.text.toString()
                        router.dialogToSearchResult(input)
                    })
                setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { _, _ -> }
                )
            }.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    companion object {
        const val TAG = "SearchDialog"
    }
}
