package com.example.moviex.presentation.screen.search

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviex.R
import com.example.moviex.databinding.FragmentSearchResBinding
import com.example.moviex.presentation.base.ui.BaseFragment
import com.example.moviex.util.delegate.RouterDelegate
import com.example.moviex.util.delegate.viewBindings
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : BaseFragment(R.layout.fragment_search_res) {

    private val viewModel by viewModel<SearchViewModel>()
    private val router by RouterDelegate()
    private val binding by viewBindings(FragmentSearchResBinding::bind)
    private val args: SearchFragmentArgs by navArgs()

    private val searchAdapter = SearchAdapter {
        router.searchResultToDetails(it.id)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setStatusBarColor(R.color.startColorGradient)
        setNavigationBarColor(R.color.endColorGradient)
        setupSearchMoviesRecycler()
        viewModel.retrieveSearchMovies(args.movieTitle)
        observeLiveData()
    }

    private fun setupSearchMoviesRecycler() {
        binding.rvSearchMovies.apply {
            adapter = searchAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        }
    }

    private fun observeLiveData() {
        viewModel.searchMoviesLiveData.observe(viewLifecycleOwner) { movies ->
            val result = movies.map {
                SearchItem(it.id, it.posterPath, it.rate, it.title, it.releaseDate)
            }
            searchAdapter.addMovies(result)
        }
    }
}
