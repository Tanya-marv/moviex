package com.example.moviex.presentation.screen.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.example.moviex.data.repository.MovieRepository
import com.example.moviex.extensions.launch
import com.example.moviex.model.Movie

class SearchViewModel(
    private val repository: MovieRepository
) : ViewModel() {

    private val _searchMoviesLiveData = MediatorLiveData<List<Movie>>()

    val searchMoviesLiveData: LiveData<List<Movie>>
        get() = _searchMoviesLiveData

    fun retrieveSearchMovies(movieTitle: String) = launch {
        repository.retrieveSearchMovies(movieTitle)
            .then { _searchMoviesLiveData.postValue(it) }
    }
}
